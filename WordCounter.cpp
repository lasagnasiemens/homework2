#include"WordCounter.h"
WordCounter::WordCounter() : value(0) {}

	void WordCounter::operator++ (int) { value++; }

	WordCounter::~WordCounter() {}

	int WordCounter::getValue() const
	{
		return value;
	}

	std::ostream & operator<<(std::ostream & st, WordCounter & wc)
	{
		return st << wc.value;
	}

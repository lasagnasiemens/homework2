#pragma once
#include<iostream>
class WordCounter{
private:
	int value;
public:
	WordCounter();
	void operator++ (int);
	friend std::ostream& operator<<(std::ostream& st, WordCounter& wc);
	bool filter(char c);
	~WordCounter();
	int getValue() const;
};
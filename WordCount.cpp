#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <string>
#include <vector>
#include "WordCounter.h"
#include <chrono>

using namespace std;
const string file = "C:\\Users\\Misu'\\Documents\\homework2\\fisier.txt";
bool filter(char c)
{
	return isalpha(c) == 0;
}
template <class T>
struct Comparator : std::binary_function<T, T, bool>
{
	inline bool operator()(const T& lhs, const T& rhs)
	{
		return lhs.second.getValue() > rhs.second.getValue();
	}
};
int main()
{
	map<string, WordCounter> words;
	map<string, WordCounter> thewords;
	ifstream input;
	typedef std::pair< string, WordCounter > word_mapping;
	input.open(file.c_str());
	if (!input)
	{
		cout << "Error in opening file\n";
		return 0;
	}

	string tok;

	chrono::steady_clock::time_point start3;
	chrono::steady_clock::time_point finish3;

	auto start = chrono::high_resolution_clock::now();
	while (true)
	{
		string newtok;
		input >> tok;
		tok.resize(remove_if(tok.begin(), tok.end(), filter) - tok.begin());

		chrono::steady_clock::time_point start3 = chrono::steady_clock::now();
		if (input)
		{
			newtok = tok + " ";
			words[tok]++;
		}
		else break;
		chrono::steady_clock::time_point finish3 = chrono::steady_clock::now();
		input >> tok;
		

		if (input)
		{
			words[tok]++;
			newtok += tok;
			thewords[newtok]++;
		}
		else break;
	}
	auto finish = chrono::high_resolution_clock::now();

	map< string, WordCounter, less<string> >::iterator it = words.begin();

	for (; it != words.end(); it++)
	{
		cout << setw(10)<< (*it).first<< "\t"<< (*it).second<< endl;
	}

	cout << "Cele mai folosite 3 cuvinte sunt:\n";
	vector< word_mapping > result(words.begin(), words.end());
	sort(result.begin(), result.end(), Comparator<word_mapping>());

	for (std::vector< word_mapping >::const_iterator mit = result.begin();
		mit != result.begin()+3;
		mit++)
	{
		cout << std::setw(15)<< (*mit).first<< "\t"<< (*mit).second.getValue()<< endl;
	}
	std::cout << "Cele mai folosita secventa de 2 cuvinte este:\n";
	std::vector< word_mapping > result2(thewords.begin(), thewords.end());
	std::sort(result2.begin(), result2.end(), Comparator<word_mapping>());

	for (std::vector< word_mapping >::const_iterator mit = result2.begin();
		mit != result2.begin()+1;
		mit++)
	{
		cout << std::setw(15) << (*mit).first << "\t" << (*mit).second.getValue() << endl;
	}

	string wordToFind;
	cout << "Word to find:"; cin >> wordToFind;
	it =words.find(wordToFind);
	auto start2 = chrono::high_resolution_clock::now();
	if (it != words.end())
	{
		cout << "Word found " << endl;
	}
	else
	{
		cout<< "Word doesn't exists " << endl;
	}

	auto finish2 = chrono::high_resolution_clock::now();


	chrono::duration<double> elapsed = (finish - start) * 1000.0;
	chrono::duration<double> elapsed2 = (finish2 - start2) * 1000.0;
	chrono::duration<double> elapsed3 = (finish3 - start3) * 1000.0;

	cout << "construct the container time:" << elapsed.count()<< " miliseconds"<< endl;
	cout << "query if a word exists:" << elapsed2.count() << " miliseconds" << endl;
	cout << "count how many time a words appears:" << elapsed3.count() << " miliseconds << endl";

	return 0;
}